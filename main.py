from http import HTTPStatus
from flask import Flask, request, Response
import json
from bills import bills

app = Flask(__name__)


@app.route("/fetch", methods=["POST"])
def fetch_bills():
    loanNo = request.json["loanNo"]
    return_data = { # Customer Not found
            "status": "FAILURE",
            "errorCode": "01"
    }
    if loanNo in bills.keys():
        return_data = bills[loanNo]["fetch"]

    return Response(content_type="application/json", response=json.dumps(return_data), status=HTTPStatus.OK)

@app.route("/pay", methods=["POST"])
def pay_bills():
    loanNo = request.json["loanNo"]
    request_data = request.json
    temp_data = request.json.pop("loanNo")
    return_data = {
        "status": "SUCCESS",
        "acknowledgementId": "1AJI1344"
    }
    if bills[loanNo]["duplicate"]:
        return_data = {
            "status": "DUPLICATE",
            "acknowledgementId": "1AJI1344"
        }
    if bills[loanNo]["fail"]:
        return_data = {
            "status": "FAILURE",
            "acknowledgementId": ""
        }
    return Response(content_type="application/json", response=json.dumps(return_data), status=HTTPStatus.OK)


if __name__ == "__main__":
    app.run(debug=True)
