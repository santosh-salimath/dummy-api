bills = {
    "1234": {
        "fetch": {
            "status": "SUCCESS",
            "errorCode": "00",
            "customerName": "Ashok Kumar",
            "amountDue": "1290.20",
            "billDate": "2021-07-15",
            "dueDate": "2021-07-30",
            "billNumber": "TUVW1234",
            "billPeriod": "MONTHLY",
        },
        "pay": {},
        "duplicate": False,
        "fail": False
    },
    "1233": {
        "fetch": {
            "status": "FAILURE",
            "errorCode": "02"
        },
        "pay": {}
    },
    "1232": {
        "fetch": {
            "status": "FAILURE",
            "errorCode": "03"
        },
        "pay": {}
    }
}
